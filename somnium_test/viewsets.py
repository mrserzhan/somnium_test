from rest_framework import viewsets

from somnium_test.models import Entity, GROUP_TYPE_EMPLOYEE, Person
from somnium_test.serializers import EmployeeSerializer, GroupSerializer, EntitySerializer, PersonSerializer, \
    JustForCreatingViewSerializer


class PersonViewSet(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()


class EntityViewSet(viewsets.ModelViewSet):
    serializer_class = True
    queryset = Entity.objects.all()

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return EntitySerializer
        if self.action in ['create'] and not self.request.data:
            return JustForCreatingViewSerializer
        return EmployeeSerializer if self.request.data.get('type', GROUP_TYPE_EMPLOYEE) == GROUP_TYPE_EMPLOYEE else GroupSerializer


