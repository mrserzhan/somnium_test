from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from somnium_test.models import Person, Entity, GROUP_TYPE_EMPLOYEE


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('id', 'first_name', 'last_name', 'middle_name')


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ('person_id', 'position', 'parent_id')

    position = serializers.CharField(allow_null=False, allow_blank=False)
    parent_id = serializers.PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True)
    person_id = serializers.PrimaryKeyRelatedField(queryset=Person.objects.all(), write_only=True)

    def validate(self, attrs):
        attrs['type'] = GROUP_TYPE_EMPLOYEE  # this is only employee's serializer
        if isinstance(attrs.get('person_id'), Person):
            attrs['person_id'] = attrs.get('person_id').id
        if isinstance(attrs.get('parent_id'), Entity):
            attrs['parent_id'] = attrs.get('parent_id').id
        return super().validate(attrs)


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ('type', 'title', 'parent_id')

    title = serializers.CharField(allow_null=False, allow_blank=False)
    parent_id = serializers.PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True)

    def validate_type(self, value):
        if value == GROUP_TYPE_EMPLOYEE:  # group serializer can't be applied for employees
            raise ValidationError('Employees must have person and position')
        return value

    def update(self, instance, validated_data):
        validated_data.pop('type')  # restrict update type field
        return super().update(instance, validated_data)


class EntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ('id', 'type', 'person', 'position', 'title', 'parent_id', 'children')

    title = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()

    def get_title(self, entity: Entity):
        return str(entity)

    def get_children(self, entity: Entity):
        serializer = EntitySerializer(instance=entity.children, many=True)
        return serializer.data


class JustForCreatingViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ('type', 'person_id', 'position', 'title', 'parent_id')
    person_id = serializers.PrimaryKeyRelatedField(queryset=Person.objects.all(), allow_null=True)
    parent_id = serializers.PrimaryKeyRelatedField(queryset=Entity.objects.all(), allow_null=True)
