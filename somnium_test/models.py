from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=True)
    middle_name = models.CharField(max_length=255, null=True)
    photo = models.ImageField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def full_name(self):
        return '%s %s %s' % (self.last_name, self.first_name, self.middle_name)

    def __str__(self):
        return self.full_name


GROUP_TYPE_UNIVERSITY = 'university'
GROUP_TYPE_COMPANY = 'company'
GROUP_TYPE_GROUP = 'group'
GROUP_TYPE_EMPLOYEE = 'employee'
GROUP_TYPES = (
    (GROUP_TYPE_UNIVERSITY, GROUP_TYPE_UNIVERSITY),
    (GROUP_TYPE_COMPANY, GROUP_TYPE_COMPANY),
    (GROUP_TYPE_GROUP, GROUP_TYPE_GROUP),
    (GROUP_TYPE_EMPLOYEE, GROUP_TYPE_EMPLOYEE),
)


class Entity(models.Model):
    parent = models.ForeignKey('Entity', null=True, on_delete=models.CASCADE, related_name='children')
    type = models.CharField(null=False, choices=GROUP_TYPES, max_length=255)
    title = models.CharField(max_length=255, null=True)
    position = models.CharField(max_length=255, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s - %s' % (self.position, self.person.full_name) if self.type == GROUP_TYPE_EMPLOYEE else self.title

